<?php  session_start();?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Fase1</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="cover.css" rel="stylesheet">
  </head>

  <body class="text-center">
      <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
              <h3 class="navbar-brand">Fase1</h3>
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="https://github.com/rkztlx/Project-SweFi-LTW">Link</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../AboutUs/AboutUs.html">Chi siamo</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../Collabora/Collabora.html">Collabora con noi</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../index.php">Home</a>
                  </li>
                </ul>
                <?php
        if ($_SESSION['username'] != '') {
          echo '<a class ="btn btn-danger my-2 my-sm-0 pad" href="#" id="reg">'.$_SESSION['username'].'</a>';
        }
        else {
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" id="reg" href="../../register/register.html">Registrati</a>';
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" href="../../login/login.html">Accedi</a>';
        }
        ?>
        </nav>
  
        <main role="main" class="inner cover">
            <h1 class="cover-heading" style="color:#9e2005;">Creazione Fake AP & Deauth(mdk3)</h1>
            <p class="lead" style="color:black;">La prima cosa da fare è creare un fake ap, per poterlo fare
            andremo ad usare hostapd per creare l'access point, dnsmasq per gestire il server,il DHCP  
            e i DNS del nostro fake ap e iptables per gestire il nat. <br>
             <img src="./mdk3.png" width="500" height="250" id="mdk3">
            </p>
            <p class="lead" style="color:black;">
             Una volta creata la nostra rete potremmo andare ad effetturare una disconnessione costante con mdk3
            </p>


            <pre><?php
            if ($_SESSION['username'] != '') {
              echo ('Per te che sei loggato in esclusiva parte del codice che andrà a formare<br>');
              echo('il programma definitivo nel progetto SweFi');
              echo htmlentities('
              #!/bin/bash

# the internet interface
internet=eth0

# the wifi interface
phy=wlan0

# The ESSID
essid="TEST"

# bring interfaces up
ip link set dev $internet up
ip link set dev $phy up

##################
# DNSMASQ
##################
echo "
interface=$phy

bind-interfaces

# Set default gateway
dhcp-option=3,10.0.0.1

# Set DNS servers to announce
dhcp-option=6,10.0.0.1

dhcp-range=10.0.0.2,10.0.0.10,12h

no-hosts

no-resolv
log-queries
log-facility=/var/log/dnsmasq.log

# Upstream DNS server
server=8.8.8.8
server=8.8.4.4

" > tmp-dnsmasq.conf

# start dnsmasq which provides DNS relaying service
dnsmasq --conf-file=tmp-dnsmasq.conf

##################
# IPTABLES
##################

# Enable Internet connection sharing
# configuring ip forwarding
echo "1" > /proc/sys/net/ipv4/ip_forward

# configuring NAT
iptables -A FORWARD -i $internet -o $phy -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -i $phy -o $internet -j ACCEPT
iptables -t nat -A POSTROUTING -o $internet -j MASQUERADE

##################
# HOSTAPD
##################
echo "ctrl_interface=/var/run/hostapd

interface=$phy

# ESSID
ssid=$essid

driver=nl80211
auth_algs=3
channel=11
hw_mode=g

# all mac addresses allowed
macaddr_acl=0

wmm_enabled=0" > tmp-hotspot.conf

# Start hostapd in screen hostapd
echo "Start hostapd in screen hostapd"
screen -dmS hostapd hostapd tmp-hotspot.conf
              
              
              
             '); 
            }?></pre></p>

        </main>
  
        <footer class="mastfoot mt-auto">
            <p><a class="btn btn-danger" href="../Fase2/Fase2.php"  role="button">Procedi alla fase 2</a></p>
          <div class="inner">
          </div>
        </footer>
      </div>
  </body>
</html>
