<?php  session_start();?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Fase3</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <h3 class="navbar-brand">Fase3</h3>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="https://github.com/rkztlx/Project-SweFi-LTW">Link</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../../AboutUs/AboutUs.html">Chi siamo</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../Collabora/Collabora.html">Collabora con noi</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="../../index.php">Home</a>
                  </li>
              </ul>
              <?php
        if ($_SESSION['username'] != '') {
          echo '<a class ="btn btn-danger my-2 my-sm-0 pad" href="#" id="reg">'.$_SESSION['username'].'</a>';
        }
        else {
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" id="reg" href="../../register/register.html">Registrati</a>';
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" href="../../login/login.html">Accedi</a>';
        }
        ?>
            </nav>
          
          <main role="main" class="inner cover">
            <h1>useless</h1>
          <h1 class="cover-heading" style="color:#9e2005;">Attesa del responso</h1>
              <p class="lead">
                  Durante questa fase non resta che aspettare la ricezione della password decriptata sul server che è stato predisposto precedentemente da questo tool (questo procedimento puo' durare molto tempo, a seconda se il modem e' nel nostro database o meno).       
                         Nessun modem e' al sicuro!
                         <div style="height:10px"></div>
                         <img src="./WhatsApp Image 2018-11-28 at 17.54.48.jpeg" width="600" height="250" id="enjoy">
                </p>

                <p class="lead">
                <?php
                 if ($_SESSION['username'] != '') {
                  echo '
                  <div style="height:50px"></div>
                  grazie '.$_SESSION['username'].' per esserti registrato, come premio potrai accedere alla repository privata come beta tester del progetto SweFi, contatta il team tramite la form <a href="../../Collabora/Collabora.html">Collabora con noi</a> con il codice JWFSRRTS_'.$_SESSION['username'].'
                  In aggiunta puoi controllare alla sequente pagina <a href="../../Tester/ModemTester.html">Modem Tester</a> se il tuo modem è già stato analizzato e risulta vulnerabile a questo attacco
                  ';
                }
               
                ?>
                </p>
        </main>
  

      <footer class="mastfoot mt-auto">
          <p><a class="btn btn-danger" href="../Fase2/Fase2.php"  role="button"> 	&#8592; Ritorna alla fase 2</a>
            <a class="btn btn-danger" href="../../index.php" role="button">Ritorna alla home	&#8594;</a></p>
        <div class="inner">
        </div>
      </footer>
    </div>

  </body>
</html>
