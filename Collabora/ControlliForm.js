function validaCollaboraForm(){
    if((!controllNome() && !controllEmail() && !controllData)) {
        alert('Alcuni campi sono errati');
        
        return false;
    } 
    else {
        alert("Form inviata corettamente, verrai contattato appena possibile");
        localStorage.setItem('nome_utente', document.getElementById("Utente").value);
        localStorage.setItem('email_utente', document.getElementById("Email").value);
        localStorage.setItem('conoscenza_utente', document.getElementById("Conoscenza").value);
        localStorage.setItem('ore1_utente', document.getElementById("ore1").value);
        localStorage.setItem('ore2_utente', document.getElementById("ore2").value);
        localStorage.setItem('ore3_utente', document.getElementById("ore3").value);
        localStorage.setItem('lavoro1_utente', document.getElementById("lunedi").checked);
        localStorage.setItem('lavoro2_utente', document.getElementById("martedi").checked);
        localStorage.setItem('lavoro3_utente', document.getElementById("mercoledi").checked);
        localStorage.setItem('lavoro4_utente', document.getElementById("giovedi").checked);
        localStorage.setItem('lavoro5_utente', document.getElementById("venerdi").checked);
        localStorage.setItem('lavoro6_utente', document.getElementById("sabato").checked);
        localStorage.setItem('lavoro7_utente', document.getElementById("domenica").checked);
        localStorage.setItem('linguaggio1_utente', document.getElementById("python").checked);
        localStorage.setItem('linguaggio2_utente', document.getElementById("java").checked);
        localStorage.setItem('linguaggio3_utente', document.getElementById("c").checked);
        localStorage.setItem('linguaggio4_utente', document.getElementById("asm").checked);
        localStorage.setItem('linguaggio5_utente', document.getElementById("html").checked);
        localStorage.setItem('linguaggio6_utente', document.getElementById("js").checked);
        localStorage.setItem('linguaggio7_utente', document.getElementById("android").checked);
        localStorage.setItem('linguaggio8_utente', document.getElementById("ios").checked);
        localStorage.setItem('linguaggio9_utente', document.getElementById("bash").checked);
        localStorage.setItem('linguaggio10_utente', document.getElementById("linguaggio").checked);
        localStorage.setItem('data', document.getElementById("data").value);
        localStorage.setItem('info', document.getElementById("info").value);
        localStorage.setItem('code', document.getElementById("secret").checked);
        
        return true;   
    }; 
}

function controllNome(){
    if(document.getElementById("Utente").value == ''){
        alert("Inserisci un nome valido");
        return false;
    } 
    else return true;
}

function controllEmail(){
    if(document.getElementById("Email").value != document.getElementById("EmailC").value ){
        alert("Hai inserito un email sbaglata o le mail non coincidono");
        return false;
    } 
    else{
       
        return true;
    }
}

function controllCode(){
    if(document.getElementById("secret").value == 'JWFSRRTS_' + $_SESSION['username']) return true;
    else return false;
}

function controllData(){
    var data = document.getElementById("data").value;
    if (data.substring(2,3) != "/" || data.value.substring(5,6) != "/" ||
    isNaN(data.substring(0,2)) ||
    isNaN(data.substring(3,5)) ||
    isNaN(data.substring(6,10))) {
	    alert("Inserire data in formato gg/mm/aaaa");
	    data = "";
	    data.focus();
	    return false;
    }
    else if (data.substring(3,5) > 12) {
        alert("Impossibile utilizzare un valore superiore a 12 per i mesi");
        data = "";
        data.focus();
        return false;
    } else if (data.substring(6,10) < 2017) {
        alert("Impossibile utilizzare un valore inferiore a 2017 per l'anno");
        data = "";
        data.focus();
        return false;
    }
    else{
        return true;
    }
}

