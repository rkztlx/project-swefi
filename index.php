<?php  session_start();?>
<!doctype html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Project SweFi</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="jumbotron.css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="js/browserDetect.js"></script>
    <script type="text/javascript" language="javascript" src="js/accesso.js"></script>
      
 
  </head>
  <body style="background-color:rgb(201, 198, 198);">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <h3 class="navbar-brand">Project SweFi</h3>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="https://github.com/rkztlx/Project-SweFi-LTW">Link</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="AboutUs/AboutUs.html">Chi siamo</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="Collabora/Collabora.html">Collabora con noi</a>
          </li>

          <?php
        if ($_SESSION['username'] != '') {
          echo '<li class="nav-item active">
          <a class="nav-link" href="Tester/ModemTester.html">Modem Tester</a>
        </li>';
        }
        ?>
        </ul>
        <?php
        if ($_SESSION['username'] != '') {
          echo '<a class ="btn btn-danger my-2 my-sm-0 pad" href="#" id="reg">'.$_SESSION['username'].'</a>';
        }
        else {
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" id="reg" href="register/register.html">Registrati</a>';
          echo'<a class ="btn btn-outline-danger my-2 my-sm-0 pad" href="login/login.html">Accedi</a>';
        }
        ?>
         
    </nav>

    <main role="main">

      
      <div class="jumbotron">
        <div class="container">
          <img src="/Immagini/beta-3.png" width="500" height="250" id="logo">
          <h3 class="display-3">Nessun modem è più al sicuro!</h3>
          <p id="su">Questo è un progetto puramente a scopo illustratitvo ed educativo e mostra di come sia possibile ottenere la password del modem attraverso un fakeAP, nessuno degli sviluppatori si assume alcuna responsabilità dell'uso improprio del software</p>
        </div>
      </div>

      <div class="container">
      <div class="how-section1">
        <div class="row">
            <div class="col-md-5 how-img">
                <img src="/Immagini/bottone1.png" width="200" height="250">
            </div>
            <div class="col-md-5">
              <h2 class="subheading">Fase 1</h2>
            <p>Individuare il modem su cui procedere con l'attacco e inibire l'accesso dell'utente ad esso attraverso una deautenticazione costante </p>
            <p><a class="btn btn-danger" href="/Fasi/Fase1/Fase1.php" role="button">Dettagli &raquo;</a></p></p>
            </div>
        </div>
        <div style="height:50px"></div>
        <div class="row">
            <div class="col-md-5">
                <h2>Fase 2</h2>
                  <p>Forzare l'utente a collegarsi al proprio FakeAP per poter iniettare il codice nella cache del browser del dispositivo attualmente in uso dall'utente</p>
                  <p><a class="btn btn-danger" href="Fasi/Fase2/Fase2.php" role="button">Dettagli &raquo;</a></p>
            </div>
            <div class="col-md-5 how-img">
              <img src="/Immagini/bottone2.png" width="200" height="250">
            </div>
        </div>
        <div style="height:50px"></div>
        <div class="row">
            <div class="col-md-5 how-img">
              <img src="/Immagini/bottone3.png" width="200" height="250">
            </div>
            <div class="col-md-5">
                <h2>Fase 3</h2>
                <p>Attendere la ricezione dei dati sul server indicato</p>
            <p><a class="btn btn-danger" href="Fasi/Fase3/Fase3.php" role="button">Dettagli &raquo;</a></p>
            </div>
    </div>
    </main>

    <div class="jumbotron">
      <div class="container col-md-5">
        <h3 id="Evil">Evil Twin</h3>
        <iframe id="video" src="https://www.youtube.com/embed/o8YXGDUlD4E?controls=0" style="border: None">
        </iframe>    
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src= "/assets/js/vendor/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  </body>
  <div class="pad">
    <p><a class="btn btn-danger" href="#su" id ="tornasu" role="button">Ritorna su</a></p>
  </div>
</html>


  
